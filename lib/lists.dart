// Copyright 2016 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';                   // new
import 'package:google_sign_in/google_sign_in.dart';                   // new
import 'dart:async';

final FirebaseAuth auth = FirebaseAuth.instance;
final googleSignIn = new GoogleSignIn();
FirebaseUser user;

Future<String> _testSignInwithGoogle() async
{
  GoogleSignInAccount googleUser = await googleSignIn.signIn();
  GoogleSignInAuthentication googleAuth = await googleUser.authentication;
   user = await auth.signInWithGoogle(idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);
  assert(user.email  != null);
  assert(user.displayName != null);
  print("the user is  $user");
  return 'signInwithGoogle succeeded : $user';
}

enum _MaterialListType {
  /// A list tile that contains a single line of text.
  oneLine,

  /// A list tile that contains a [CircleAvatar] followed by a single line of text.
  oneLineWithAvatar,

  /// A list tile that contains two lines of text.
  twoLine,

  /// A list tile that contains three lines of text.
  threeLine,
}

class ListDemo extends StatefulWidget {
  const ListDemo({ Key key }) : super(key: key);

  static const String routeName = '/material/list';

  @override
  _ListDemoState createState() => new _ListDemoState();
}

class _ListDemoState extends State<ListDemo> {
  static final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  PersistentBottomSheetController<Null> _bottomSheet;
  _MaterialListType _itemType = _MaterialListType.threeLine;
  bool _dense = false;
  bool _showAvatars = true;
  bool _showIcons = false;
  bool _showDividers = false;
  bool _reverseSort = false;
  bool signedIn = false;
  final String _simpleValue1 = 'Menu item value one';
  final String _simpleValue2 = 'Menu item value two';
  final String _simpleValue3 = 'Menu item value three';
  String _simpleValue;
  List<String> items = <String>[
    'A', 'B', 'C', 'D',
  ];

  void changeItemType(_MaterialListType type) {
    setState(() {
      _itemType = type;
    });
    _bottomSheet?.setState(() { });
  }

  void settings() {
    final PersistentBottomSheetController<Null> bottomSheet = scaffoldKey.currentState.showBottomSheet((BuildContext bottomSheetContext) {
      return new Container(
        decoration: const BoxDecoration(
          border: const Border(top: const BorderSide(color: Colors.black26)),
        ),
        child: new ListView(
          shrinkWrap: true,
          primary: false,
          children: <Widget>[
            new Text("this has to change"),
            new ListTile(
              dense: true,
              title: const Text('Show dividers'),
              trailing: new Checkbox(
                value: _showDividers,
                onChanged: (bool value) {
                  setState(() {
                    _showDividers = value;
                  });
                  _bottomSheet?.setState(() { });
                },
             ),
            ),
            new ListTile(
              dense: true,
              title: const Text('Dense layout'),
              trailing: new Checkbox(
                value: _dense,
                onChanged: (bool value) {
                  setState(() {
                    _dense = value;
                  });
                  _bottomSheet?.setState(() { });
                },
              ),
            ),
          ],
        ),
      );
    });

  }

  Widget buildListTile(BuildContext context, String item) {
    Widget secondary = const Text(
        "Even more additional list item information appears on line three.",
      );

    return new ListTile(
      isThreeLine: true,
      dense: _dense,
      leading: _showAvatars ? new CircleAvatar(child: new Text(item)) : null,
      title: new Text('Supermarket'),
      subtitle: secondary,
      //trailing:  new Icon(Icons.info, color: Theme.of(context).disabledColor),
    );
  }

  void _incrementCounter()
  {
      print("button pressed");
      //_testSignInwithGoogle();
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(value)
    ));
  }

  void showMenuSelection(String value) {
    //if (<String>[_simpleValue1, _simpleValue2, _simpleValue3].contains(value))
    if(_simpleValue1 == value)
      {
        _testSignInwithGoogle();
        if(user.displayName != null)
        {
          signedIn = true;
          showInSnackBar('Signed in as: ${user.displayName}');
        }
        else
          signedIn = false;
      }
    _simpleValue = value;

  }

  @override
  Widget build(BuildContext context)
  {
   Iterable<Widget> listTiles = items.map((String item) => buildListTile(context, item));
    //if (_showDividers)
      //listTiles = ListTile.divideTiles(context: context, tiles: listTiles);

    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new ListTile(
          title: new Text('Shoppy Calc',
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                            color: Colors.white,
                             fontSize: 25.0,
                             fontWeight: FontWeight.w400,
                            ),
                      ),
            trailing: new PopupMenuButton<String>(
                //padding: EdgeInsets.zero,
                onSelected: showMenuSelection,
                itemBuilder: (BuildContext context) => <PopupMenuItem<String>>[
                  new PopupMenuItem<String>(
                      value: _simpleValue1,
                      child: const Text('Sign In'),
                  ),
                  new PopupMenuItem<String>(
                      enabled: signedIn? true : false,
                      child: const Text('sign out')
                  ),
                  new PopupMenuItem<String>(
                      value: "Setting menu",
                      child: const Text('Settings')
                  ),
                ]
            )
        ),
        /*title: new Text('Scrolling list'),
        actions: <Widget>[
         /*new IconButton(
            icon: const Icon(Icons.more_vert),
            tooltip: 'Show menu',
            onPressed: _bottomSheet == null ? settings : null,
          ),*/
        ],*/
      ),
      body: new Scrollbar(
        child: new ListView(
          padding: new EdgeInsets.symmetric(vertical: _dense ? 4.0 : 8.0),
          children: listTiles.toList(),
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: new Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
